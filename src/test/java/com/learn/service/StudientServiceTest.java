package com.learn.service;



import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.learn.enemuration.Gender;
import com.learn.entity.Studient;
import com.learn.repository.StudientRepo;



@ExtendWith(MockitoExtension.class)
class StudientServiceTest {
	@Mock
	StudientRepo repo;
	@InjectMocks
	StudientService service;

//	@BeforeEach
//	void setUp()  {
//		 service = new StudientService(repo);
//	}

	
	@Test
	void canGetAllStudents() {
		service.getAllStudents();
		verify(repo).findAll();
	}

	@Test

	void testAddStudent() {
	
		//given
		  String email = "mahjoub@mahjoub";
		  Studient st = new Studient("mahjoub", email , Gender.MALE);
		//when
		  service.addStudent(st);
		//then
		  
		 
//		 ArgumentCaptor<Studient> studientCaptor = ArgumentCaptor.forClass(Studient.class);
//		 verify(repo).save(studientCaptor.capture());
//		 Studient expectedStudient = studientCaptor.getValue();
//		 assertThat(expectedStudient).isEqualTo(st);
		 	 
	}
	
	@Test
	void willThrowWhenEmailIsTaken() {
		//given
		 String email = "mahjoub@mahjoub";
		  Studient st = new Studient("mahjoub", email , Gender.MALE);
		//when
		  given(repo.selectExistsEmail(anyString()))
          .willReturn(true);
		//then
		   assertThatThrownBy(()->service.addStudent(st))
		  .isInstanceOf(RuntimeException.class)
		   .hasMessageContaining("Email " + st.getEmail() + " taken");

		   verify(repo,never()).save(any());
	}

	@Test
	void canDeleteStudent() {
	//given
		long id = 1;
	//when
		given(repo.existsById(id)).willReturn(true);
		service.deleteStudent(id);
		
	//then
		ArgumentCaptor<Long> idCaptor 
		= ArgumentCaptor.forClass(Long.class);
	   verify(repo).deleteById(idCaptor.capture());
	   Long expected = idCaptor.getValue();
	   assertThat(expected).isEqualTo(id);
	}
	
	@Test
	void willThrowWhenStudientIdIsNotExist() {
		//given
		long id = 10;
		//when
		  given(repo.existsById(any())).willReturn(false);
          
		//then
		   assertThatThrownBy(()->service.deleteStudent(id))
		  .isInstanceOf(RuntimeException.class)
		   .hasMessageContaining( "Student with id " + id + " does not exists");

		   verify(repo,never()).save(any());
	}

	
	

}
