package com.learn.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.learn.enemuration.Gender;
import com.learn.entity.Studient;

@DataJpaTest

class StudientRepoTest {

	@Autowired
	private StudientRepo underTest;

	@BeforeEach
	void setUp() {
	}
	
	@AfterEach
	void tearDown() {
		underTest.deleteAll();
	}

	@Test
	void itshouldTestIfStudientEmailExist() {

		// given
         String email = "mahjoub@mahjoub";
		Studient st = new Studient("mahjoub", email , Gender.MALE);
		underTest.save(st);
		// when
	  	boolean result = underTest.selectExistsEmail(email);
		// then
		assertThat(result).isTrue();

	}
	
	@Test
	void itshouldTestIfStudientEmailDoesNotExist() {

		// given
		  String email = "mahjoub@mahjoub";
		
	
		// when
	  	boolean result = underTest.selectExistsEmail(email);
		// then
		assertThat(result).isFalse();
		
	}

}
