package com.learn.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.learn.entity.Studient;
import com.learn.repository.StudientRepo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StudientService {
	

    private final StudientRepo studentRepository;

    public List<Studient> getAllStudents() {
        return studentRepository.findAll();
    }

    public void addStudent(Studient student) {
        Boolean existsEmail = studentRepository
                .selectExistsEmail(student.getEmail());
        if (existsEmail) {
            throw new RuntimeException(
                    "Email " + student.getEmail() + " taken");
        }

        studentRepository.save(student);
    }

    public String deleteStudent(Long studentId) {
        if(!studentRepository.existsById(studentId)) {
            throw new RuntimeException(
                    "Student with id " + studentId + " does not exists");
        }
        studentRepository.deleteById(studentId);
        return "le studient id :  "+ studentId + "est supprimé avec succés" ;
    }

}
