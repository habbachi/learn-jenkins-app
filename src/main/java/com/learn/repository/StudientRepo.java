package com.learn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.learn.entity.Studient;

public interface StudientRepo extends JpaRepository<Studient,Long>{

	  @Query("" +
	            "SELECT CASE WHEN COUNT(s) > 0 THEN " +
	            "TRUE ELSE FALSE END " +
	            "FROM Studient s " +
	            "WHERE s.email = ?1"
	    )
	    Boolean selectExistsEmail(String email);
}
